#!/bin/bash

# define current dir
home_dir="/home/ubuntu"
dump_dir="/home/ubuntu/dumps"

# define psql password
export PGPASSWORD="3422-16gh-f45y-asdf"

# get the execution timestamp
START_TIME_STAMP=$(date +"%Y%m%d%H%M")

#find the latest dump name
OUTPUT="$(/usr/local/bin/aws s3 ls heroku-database-backups-production/hellogold-wallstreet-prod-th-8/DATABASE_URL --recursive | sort | tail -n 1 | awk '{print $4}')"

# copy latest backup from s# to home dir 
/usr/local/bin/aws s3 cp s3://heroku-database-backups-production/${OUTPUT} ${dump_dir}/${START_TIME_STAMP}backup.dump.gz

# unzip the dump file to a specific directory
gunzip -c ${dump_dir}/${START_TIME_STAMP}backup.dump.gz > ${dump_dir}/${START_TIME_STAMP}backup.dump

# drop database if exists
dropdb --if-exists -h datawarehouse-ws-th.cje84fsfzqlx.ap-southeast-1.rds.amazonaws.com -p 18891 -U datateam useradmin_development

# create database
createdb -h datawarehouse-ws-th.cje84fsfzqlx.ap-southeast-1.rds.amazonaws.com -p 18891 -U datateam useradmin_development

# restore dump file to database
pg_restore --verbose --clean --no-acl --no-owner -h datawarehouse-ws-th.cje84fsfzqlx.ap-southeast-1.rds.amazonaws.com -p 18891 -U datateam -d useradmin_development ${dump_dir}/${START_TIME_STAMP}backup.dump

# delete temporary dump files
cd dumps/ && rm -rf *.*